package nzhi.dnt.epresensiblora.bridge;

import com.google.firebase.analytics.FirebaseAnalytics;

public class AppConfig {
    public static String HOST = "https://simpeg.blorakab.go.id/presensi/api/";
    public static String URL_LaporanBulan = (HOST + "laporanperbulan");
    public static String URL_LaporanTahun = (HOST + "laporanpertahun");
    public static String URL_LaporanTanggal = (HOST + "laporanpertanggal");
    public static String URL_Login = (HOST + FirebaseAnalytics.Event.LOGIN);
    public static String URL_ReqHome = (HOST + "datahariini");
    public static String URL_ReqLapAll = (HOST + "laporanperbulan");
    public static String URL_ReqLokasi = (HOST + "newlokasiabsen");
    public static String URL_SendAbsen = (HOST + "sendabsen");
    public static String URL_SendAbsenManual = (HOST + "sendinputmodekhusus");
    public static String api_key = "cHJlc.2Vuc2kga2FidXBhdGVuIGJsb3JhIGFwaQ";
}
