package nzhi.dnt.epresensiblora.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class IMEIUtil {
    public static String getDeviceId(Context context) {
        @SuppressLint("MissingPermission") String trim = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId().trim();
        if (trim == null) {
            trim = Build.SERIAL + "#" + Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        return trim.trim();
    }
}
