package nzhi.dnt.epresensiblora.helper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import nzhi.dnt.epresensiblora.R;

public class GPSTracker extends Service implements LocationListener {
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 60000;
    private static String TAG = "nzhi.dnt.epresensiblora.helper.GPSTracker";
    boolean canGetLocation = false;
    int geocoderMaxResults = 1;
    boolean isGPSEnabled = false;
    boolean isGPSTrackingEnabled = false;
    boolean isNetworkEnabled = false;
    double latitude;
    Location location;
    protected LocationManager locationManager;
    double longitude;
    /* access modifiers changed from: private */
    public final Context mContext;
    private String provider_info;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public GPSTracker(Context context) {
        this.mContext = context;
        getLocation();
    }

    @SuppressLint({"MissingPermission"})
    public void getLocation() {
        try {
            this.locationManager = (LocationManager) this.mContext.getSystemService(FirebaseAnalytics.Param.LOCATION);
            this.isGPSEnabled = this.locationManager.isProviderEnabled("gps");
            this.isNetworkEnabled = this.locationManager.isProviderEnabled("network");
            if (this.isGPSEnabled) {
                this.isGPSTrackingEnabled = true;
                Log.d(TAG, "Application use GPS Service");
                this.provider_info = "gps";
            } else if (this.isNetworkEnabled) {
                this.isGPSTrackingEnabled = true;
                Log.d(TAG, "Application use Network State to get GPS coordinates");
                this.provider_info = "network";
            }
            if (!this.provider_info.isEmpty()) {
                this.locationManager.requestLocationUpdates(this.provider_info, MIN_TIME_BW_UPDATES, 10.0f, this);
                if (this.locationManager != null) {
                    this.location = this.locationManager.getLastKnownLocation(this.provider_info);
                    updateGPSCoordinates();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Impossible to connect to LocationManager", e);
        }
    }

    public void updateGPSCoordinates() {
        Location location2 = this.location;
        if (location2 != null) {
            this.latitude = location2.getLatitude();
            this.longitude = this.location.getLongitude();
        }
    }

    public double getLatitude() {
        Location location2 = this.location;
        if (location2 != null) {
            this.latitude = location2.getLatitude();
        }
        return this.latitude;
    }

    public double getLongitude() {
        Location location2 = this.location;
        if (location2 != null) {
            this.longitude = location2.getLongitude();
        }
        return this.longitude;
    }

    public boolean getIsGPSTrackingEnabled() {
        return this.isGPSTrackingEnabled;
    }

    public void stopUsingGPS() {
        LocationManager locationManager2 = this.locationManager;
        if (locationManager2 != null) {
            locationManager2.removeUpdates(this);
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle(R.string.GPSAlertDialogTitle);
        builder.setMessage(R.string.GPSAlertDialogMessage);
        builder.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                GPSTracker.this.mContext.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public List<Address> getGeocoderAddress(Context context) {
        if (this.location == null) {
            return null;
        }
        try {
            return new Geocoder(context, Locale.ENGLISH).getFromLocation(this.latitude, this.longitude, this.geocoderMaxResults);
        } catch (IOException e) {
            Log.e(TAG, "Impossible to connect to Geocoder", e);
            return null;
        }
    }

    public String getAddressLine(Context context) {
        List<Address> geocoderAddress = getGeocoderAddress(context);
        if (geocoderAddress == null || geocoderAddress.size() <= 0) {
            return null;
        }
        return geocoderAddress.get(0).getAddressLine(0);
    }

    public String getLocality(Context context) {
        List<Address> geocoderAddress = getGeocoderAddress(context);
        if (geocoderAddress == null || geocoderAddress.size() <= 0) {
            return null;
        }
        return geocoderAddress.get(0).getLocality();
    }

    public String getPostalCode(Context context) {
        List<Address> geocoderAddress = getGeocoderAddress(context);
        if (geocoderAddress == null || geocoderAddress.size() <= 0) {
            return null;
        }
        return geocoderAddress.get(0).getPostalCode();
    }

    public String getCountryName(Context context) {
        List<Address> geocoderAddress = getGeocoderAddress(context);
        if (geocoderAddress == null || geocoderAddress.size() <= 0) {
            return null;
        }
        return geocoderAddress.get(0).getCountryName();
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public void onLocationChanged(Location location2) {
        this.location = location2;
        getLatitude();
        getLongitude();
    }
}
