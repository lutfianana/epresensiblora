package nzhi.dnt.epresensiblora.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserHelper_sqlite extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "EpresensiBlora";
    private static final int DATABASE_VERSION = 1;

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public UserHelper_sqlite(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS user_member(nip VARCHAR, nama VARCHAR, nohp VARCHAR, password VARCHAR, loggedin_token TEXT, foto VARCHAR, skpd VARCHAR, lat TEXT, lang TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS tb_longlat_now(nip VARCHAR, lat_now TEXT, long_now TEXT);");
    }

    public void saveuser(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        String replaceAll = str2.replaceAll("'", "\\'");
        writableDatabase.execSQL("INSERT INTO user_member(nip, nama, nohp, loggedin_token, foto, skpd, lat, lang) VALUES ('" + str + "','" + replaceAll + "','" + str3 + "','" + str4 + "','" + str5 + "','" + str6 + "','" + str7 + "','" + str8 + "');");
        writableDatabase.close();
    }

    public void savelonglatnow(String str, String str2, String str3) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("INSERT INTO tb_longlat_now(nip, lat_now, long_now) VALUES ('" + str + "','" + str2 + "','" + str3 + "');");
        writableDatabase.close();
    }

    public void uplonglatnow(String str, String str2, String str3) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("UPDATE tb_longlat_now SET lat_now = '" + str2 + "', long_now = '" + str3 + "' WHERE nip = '" + str + "'");
        writableDatabase.close();
    }

    public String getobject(String str) {
        String str2 = null;
        Cursor rawQuery = getWritableDatabase().rawQuery("SELECT " + str + " from user_member", null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            rawQuery.close();
            return str2;
        }
        rawQuery.moveToFirst();
        do {
            str2 = rawQuery.getString(0);
        } while (rawQuery.moveToNext());
        rawQuery.close();
        return str2;
    }

    public String getlonglat(String str) {
        String str2 = null;
        Cursor rawQuery = getWritableDatabase().rawQuery("SELECT " + str + " from tb_longlat_now", null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            rawQuery.close();
            return str2;
        }
        rawQuery.moveToFirst();
        do {
            str2 = rawQuery.getString(0);
        } while (rawQuery.moveToNext());
        rawQuery.close();
        return str2;
    }

    public void delete(String str) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("DELETE FROM " + str);
        writableDatabase.close();
    }
}
