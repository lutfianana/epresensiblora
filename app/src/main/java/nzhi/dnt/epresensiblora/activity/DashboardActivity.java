package nzhi.dnt.epresensiblora.activity;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import nzhi.dnt.epresensiblora.R;
import nzhi.dnt.epresensiblora.fragment.HomeFragment;
import nzhi.dnt.epresensiblora.fragment.LaporanFragment;
import nzhi.dnt.epresensiblora.fragment.PresensiFragment;

public class DashboardActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_dashboard);
        getSupportActionBar().hide();
        String stringExtra = getIntent().getStringExtra("data");
        if (stringExtra != null && stringExtra.contentEquals("1")) {
            loadFragment(new HomeFragment());
        } else if (stringExtra == null || !stringExtra.contentEquals("2")) {
            loadFragment(new HomeFragment());
        } else {
            loadFragment(new PresensiFragment());
        }
        ((BottomNavigationView) findViewById(R.id.navigation)).setOnNavigationItemSelectedListener(this);
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment;
        switch (menuItem.getItemId()) {
            case R.id.nav_gift:
                fragment = new LaporanFragment();
                break;
            case R.id.nav_home:
                fragment = new HomeFragment();
                break;
            case R.id.nav_lokasi:
                fragment = new PresensiFragment();
                break;
            default:
                fragment = null;
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment == null) {
            return false;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        return true;
    }
}
