package nzhi.dnt.epresensiblora.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import nzhi.dnt.epresensiblora.R;
import nzhi.dnt.epresensiblora.model.item_data;

public class adapter_laporan_tahunan extends RecyclerView.Adapter<adapter_laporan_tahunan.MyViewHolder> {
    private List<item_data> ListMenu;
    private Context mContext;

    public int getItemViewType(int i) {
        return i;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvbulan;
        public final TextView tvjumlahabsen;
        public final TextView tvjumlahmasuk;

        public MyViewHolder(View view) {
            super(view);
            this.tvbulan = view.findViewById(R.id.tvnamabulan);
            this.tvjumlahmasuk = view.findViewById(R.id.tvjmlhadirtaun);
            this.tvjumlahabsen = view.findViewById(R.id.tvjmlabsentaun);
        }
    }

    public adapter_laporan_tahunan(Context context, List<item_data> list) {
        this.mContext = context;
        this.ListMenu = list;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_laporan_tahun, viewGroup, false));
    }

    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        String str;
        item_data item_data = this.ListMenu.get(i);
        if (item_data.getNamaBulan().equals("0")) {
            ViewGroup.LayoutParams layoutParams = myViewHolder.itemView.getLayoutParams();
            layoutParams.height = 0;
            myViewHolder.itemView.setLayoutParams(layoutParams);
        }
        if (item_data.getNamaBulan().equals("1")) {
            str = "Januari";
        } else if (item_data.getNamaBulan().equals("2")) {
            str = "Februari";
        } else if (item_data.getNamaBulan().equals("3")) {
            str = "Maret";
        } else if (item_data.getNamaBulan().equals("4")) {
            str = "April";
        } else if (item_data.getNamaBulan().equals("5")) {
            str = "Mei";
        } else if (item_data.getNamaBulan().equals("6")) {
            str = "Juni";
        } else if (item_data.getNamaBulan().equals("7")) {
            str = "Juli";
        } else if (item_data.getNamaBulan().equals("8")) {
            str = "Agustus";
        } else if (item_data.getNamaBulan().equals("9")) {
            str = "September";
        } else if (item_data.getNamaBulan().equals("10")) {
            str = "Oktober";
        } else if (item_data.getNamaBulan().equals("11")) {
            str = "November";
        } else {
            str = item_data.getNamaBulan().equals("12") ? "Desember" : null;
        }
        myViewHolder.tvbulan.setText(str);
        myViewHolder.tvjumlahabsen.setText(item_data.getJumlahAbsen());
        myViewHolder.tvjumlahmasuk.setText(item_data.getJumlahMasuk());
    }

    public int getItemCount() {
        return this.ListMenu.size();
    }

    private void setMargins(View view, int i, int i2, int i3, int i4) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).setMargins(i, i2, i3, i4);
            view.requestLayout();
        }
    }
}
