package nzhi.dnt.epresensiblora.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nzhi.dnt.epresensiblora.R;
import nzhi.dnt.epresensiblora.activity.DashboardActivity;
import nzhi.dnt.epresensiblora.bridge.AppConfig;
import nzhi.dnt.epresensiblora.bridge.AppController;
import nzhi.dnt.epresensiblora.helper.UserHelper_sqlite;
import nzhi.dnt.epresensiblora.model.item_data;
import org.json.JSONException;
import org.json.JSONObject;

public class adapter_lokasi extends RecyclerView.Adapter<adapter_lokasi.MyViewHolder> {
    static String TAG = "AdapterLokasi";
    private List<item_data> ListMenu;
    DateFormat df = new SimpleDateFormat("yyy-MM-dd");
    DateFormat djam = new SimpleDateFormat("HH:mm:ss");
    /* access modifiers changed from: private */
    public Context mContext;
    ProgressDialog pDialog;
    String strjam = this.djam.format(this.today);
    String strtanggal = this.df.format(this.today);
    Date today = Calendar.getInstance().getTime();
    UserHelper_sqlite userHelper_sqlite;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout LLPULANG;
        public final Button btnabsenmasuk;
        public final Button btnabsenpulang;
        public final ImageView imglokasi;
        public final TextView tvalamat;
        public final TextView tvjammasuk;
        public final TextView tvjampulang;
        public final TextView tvjenisabsen;
        public final TextView tvnama;
        public final TextView tvnamamasuk;

        public MyViewHolder(View view) {
            super(view);
            this.tvjenisabsen = view.findViewById(R.id.tvjenis);
            this.tvnama = view.findViewById(R.id.tvnama);
            this.tvalamat = view.findViewById(R.id.tvalamat);
            this.imglokasi = view.findViewById(R.id.imglokasi);
            this.tvjammasuk = view.findViewById(R.id.tvjammasuk);
            this.tvjampulang = view.findViewById(R.id.tvjampulang);
            this.btnabsenmasuk = view.findViewById(R.id.btnabsenmasuk);
            this.btnabsenpulang = view.findViewById(R.id.btnabsenpulang);
            this.LLPULANG = view.findViewById(R.id.LLpulang);
            this.tvnamamasuk = view.findViewById(R.id.tvnamamasuk);
        }
    }

    public adapter_lokasi(Context context, List<item_data> list) {
        this.pDialog = new ProgressDialog(context);
        this.mContext = context;
        this.ListMenu = list;
        this.userHelper_sqlite = new UserHelper_sqlite(this.mContext.getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        hidePDialog();
    }

    private void showPDialog(String str) {
        if (!this.pDialog.isShowing()) {
            this.pDialog.setMessage(str);
        }
        this.pDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePDialog() {
        if (this.pDialog.isShowing()) {
            this.pDialog.dismiss();
        }
    }

    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_list_lokasi, viewGroup, false));
    }

    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        item_data item_data = this.ListMenu.get(i);
        if (Double.valueOf(Double.parseDouble(item_data.getJarak())).doubleValue() > 100.0d) {
            ViewGroup.LayoutParams layoutParams = myViewHolder.itemView.getLayoutParams();
            layoutParams.height = 0;
            myViewHolder.itemView.setLayoutParams(layoutParams);
        }
        if (item_data.getJenisAbsen().equals("reguler")) {
            myViewHolder.tvnama.setText(item_data.getNamaLok());
            myViewHolder.tvalamat.setText(item_data.getAlamat());
            if (!item_data.getStatusAbsenMasuk().equals("0")) {
                myViewHolder.btnabsenmasuk.setVisibility(View.GONE);
                myViewHolder.tvjammasuk.setVisibility(View.VISIBLE);
                myViewHolder.tvjammasuk.setText(item_data.getJamDatang());
            }
            if (!item_data.getStatusAbsenPulang().equals("0")) {
                myViewHolder.btnabsenpulang.setVisibility(View.GONE);
                myViewHolder.tvjampulang.setVisibility(View.VISIBLE);
                myViewHolder.tvjampulang.setText(item_data.getJamPulang());
            }
            myViewHolder.btnabsenmasuk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    adapter_lokasi.this.SendAbsen();
                }
            });
            myViewHolder.btnabsenpulang.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    adapter_lokasi.this.SendAbsen();
                }
            });
        } else if (item_data.getJenisAbsen().equals("terjadwal")) {
            myViewHolder.tvjenisabsen.setVisibility(View.VISIBLE);
            myViewHolder.tvnama.setText(item_data.getNamaLok());
            myViewHolder.tvalamat.setText(item_data.getAlamat());
            if (!item_data.getStatusAbsenMasuk().equals("0")) {
                myViewHolder.btnabsenmasuk.setVisibility(View.GONE);
                myViewHolder.tvjammasuk.setVisibility(View.VISIBLE);
                myViewHolder.tvjammasuk.setText(item_data.getJamDatang());
            }
            if (!item_data.getStatusAbsenPulang().equals("0")) {
                myViewHolder.btnabsenpulang.setVisibility(View.GONE);
                myViewHolder.tvjampulang.setVisibility(View.VISIBLE);
                myViewHolder.tvjampulang.setText(item_data.getJamPulang());
            }
            myViewHolder.btnabsenmasuk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    adapter_lokasi.this.SendAbsen();
                }
            });
            myViewHolder.btnabsenpulang.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    adapter_lokasi.this.SendAbsen();
                }
            });
        } else if (item_data.getJenisAbsen().equals("request")) {
            myViewHolder.tvnama.setText(item_data.getNamaLok());
            myViewHolder.tvalamat.setText(item_data.getAlamat());
            myViewHolder.LLPULANG.setVisibility(View.GONE);
            myViewHolder.tvnamamasuk.setVisibility(View.GONE);
            if (!item_data.getStatusAbsenMasuk().equals("1")) {
                ViewGroup.LayoutParams layoutParams2 = myViewHolder.itemView.getLayoutParams();
                layoutParams2.height = 0;
                myViewHolder.itemView.setLayoutParams(layoutParams2);
            } else if (item_data.getStatVerifikasi().equals("0")) {
                myViewHolder.btnabsenmasuk.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.shape_button_red));
                myViewHolder.btnabsenmasuk.setText("Dalam Verifikasi");
            } else {
                myViewHolder.btnabsenmasuk.setVisibility(View.GONE);
                myViewHolder.tvjammasuk.setVisibility(View.VISIBLE);
                myViewHolder.tvjammasuk.setText(item_data.getJamDatang());
            }
        }
    }

    public int getItemCount() {
        return this.ListMenu.size();
    }

    private void setMargins(View view, int i, int i2, int i3, int i4) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).setMargins(i, i2, i3, i4);
            view.requestLayout();
        }
    }

    /* access modifiers changed from: private */
    public void SendAbsen() {
        showPDialog("Loading ..");
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_SendAbsen, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                adapter_lokasi.this.hidePDialog();
                try {
                    String string = new JSONObject(str).getString("response");
                    if (string.equals("1")) {
                        adapter_lokasi.this.DialogPesan("Sukses", "Berhasil Input Absen");
                    } else if (string.equals("4")) {
                        adapter_lokasi.this.DialogPesan("Warning", "Maaf, Anda tidak memiliki jadwal Dinas Hari ini");
                    } else {
                        adapter_lokasi.this.DialogPesanGagal("Gagal", "Maaf, Terjadi Kesalahan. Mohon Ulangi");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String str = adapter_lokasi.TAG;
                VolleyLog.d(str, "Error: " + volleyError.getMessage());
                adapter_lokasi.this.hidePDialog();
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("loggedin_token", adapter_lokasi.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("nip", adapter_lokasi.this.userHelper_sqlite.getobject("nip"));
                hashMap.put("tanggal", adapter_lokasi.this.strtanggal);
                hashMap.put("jam", adapter_lokasi.this.strjam);
                hashMap.put("api_key", AppConfig.api_key);
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void DialogPesan(String str, String str2) {
        new MaterialDialog.Builder(this.mContext).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                Intent intent = new Intent(adapter_lokasi.this.mContext, DashboardActivity.class);
                intent.putExtra("data", "2");
                adapter_lokasi.this.mContext.startActivity(intent);
                ((Activity) adapter_lokasi.this.mContext).finish();
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void DialogPesanGagal(String str, String str2) {
        new MaterialDialog.Builder(this.mContext).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
            }
        }).show();
    }
}
