package nzhi.dnt.epresensiblora.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import nzhi.dnt.epresensiblora.BuildConfig;
import nzhi.dnt.epresensiblora.R;
import nzhi.dnt.epresensiblora.activity.DashboardActivity;
import nzhi.dnt.epresensiblora.activity.InputMapsActivity;
import nzhi.dnt.epresensiblora.activity.LoginActivity;
import nzhi.dnt.epresensiblora.adapter.adapter_lokasi;
import nzhi.dnt.epresensiblora.bridge.AppConfig;
import nzhi.dnt.epresensiblora.bridge.AppController;
import nzhi.dnt.epresensiblora.helper.UserHelper_sqlite;
import nzhi.dnt.epresensiblora.model.item_data;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PresensiFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int REQUEST_CHECK_SETTINGS = 10000;
    public static final String TAG = "Foo";
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    LinearLayout LLalert;
    LinearLayout LLibur;
    LinearLayout LLokasi;
    private List<item_data> ListMenu = new ArrayList();
    private RecyclerView RVmenu;
    public Activity activity;
    List<Address> addresses;
    CountDownTimer countDownTimer;
    DateFormat df;
    DateFormat dlokal;
    Geocoder geocoder;
    Double latsekarang;
    Double longsekarang;
    adapter_lokasi mAdapter;
    /* access modifiers changed from: private */
    public Context mContext;
    protected LayoutManagerType mCurrentLayoutManagerType;
    /* access modifiers changed from: private */
    public Location mCurrentLocation;
    /* access modifiers changed from: private */
    public FusedLocationProviderClient mFusedLocationClient;
    /* access modifiers changed from: private */
    public String mLastUpdateTime;
    protected RecyclerView.LayoutManager mLayoutManager;
    /* access modifiers changed from: private */
    public LocationCallback mLocationCallback;
    LocationRequest mLocationRequest;
    LocationSettingsRequest mLocationSettingsRequest;
    /* access modifiers changed from: private */
    public Boolean mRequestingLocationUpdates;
    private SettingsClient mSettingsClient;
    ProgressDialog pDialog;
    View rootView;
    String strlat;
    String strlong;
    String strtanggal;
    SwipeRefreshLayout swipeLayout;
    Date today;
    TextView tvlokasi;
    TextView tvtanggal;
    TextView tvtimer;
    TextView tvupdateon;
    UserHelper_sqlite userHelper_sqlite;

    private enum LayoutManagerType {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.rootView = layoutInflater.inflate(R.layout.fragment_presensi, viewGroup, false);
        this.pDialog = new ProgressDialog(getActivity());
        this.userHelper_sqlite = new UserHelper_sqlite(getActivity().getApplication());
        this.geocoder = new Geocoder(getActivity(), Locale.getDefault());
        this.tvtanggal = this.rootView.findViewById(R.id.tvtanggal);
        this.LLalert = this.rootView.findViewById(R.id.LLarea);
        this.LLibur = this.rootView.findViewById(R.id.LLibur);
        this.tvtimer = this.rootView.findViewById(R.id.tvtimer);
        initlokasi();
        restoreValuesFromBundle(bundle);
        startLocationButtonClick();
        this.df = new SimpleDateFormat("yyyy-MM-dd");
        this.dlokal = new SimpleDateFormat("EEE, dd MMM yyyy");
        this.today = Calendar.getInstance().getTime();
        this.strtanggal = this.df.format(this.today);
        this.tvtanggal.setText(this.dlokal.format(this.today));
        this.geocoder = new Geocoder(getContext(), Locale.getDefault());
        this.LLokasi = this.rootView.findViewById(R.id.LLokasi);
        this.LLokasi.setVisibility(8);
        this.RVmenu = this.rootView.findViewById(R.id.RVList);
        this.RVmenu.setHasFixedSize(true);
        this.mLayoutManager = new GridLayoutManager(getContext(), 1);
        this.RVmenu.setLayoutManager(this.mLayoutManager);
        this.mAdapter = new adapter_lokasi(getContext(), this.ListMenu);
        this.RVmenu.setAdapter(this.mAdapter);
        ReqList();
        this.swipeLayout = this.rootView.findViewById(R.id.swipe_container);
        this.swipeLayout.setOnRefreshListener(this);
        this.swipeLayout.setColorSchemeColors(getContext().getResources().getColor(17170453), getContext().getResources().getColor(17170455), getContext().getResources().getColor(17170451), getContext().getResources().getColor(17170457));
        this.countDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long j) {
            }

            public void onFinish() {
                PresensiFragment.this.DialogPesanExpired("Peringatan", "Sesi Anda Telah Berakhir, Silakan Refresh");
            }
        }.start();
        return this.rootView;
    }

    public void onDestroy() {
        super.onDestroy();
        this.countDownTimer.cancel();
        hidePDialog();
    }

    public void onStop() {
        super.onStop();
        this.countDownTimer.cancel();
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.countDownTimer.cancel();
    }

    private void showPDialog(String str) {
        if (!this.pDialog.isShowing()) {
            this.pDialog.setMessage(str);
        }
        this.pDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePDialog() {
        if (this.pDialog.isShowing()) {
            this.pDialog.dismiss();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(PresensiFragment.this.getActivity(), DashboardActivity.class);
                intent.putExtra("data", "2");
                PresensiFragment.this.startActivity(intent);
                PresensiFragment.this.getActivity().finish();
            }
        }, 1500);
    }

    /* access modifiers changed from: private */
    public void DialogPesanGagal(String str, String str2) {
        new MaterialDialog.Builder(this.mContext).title(str).content(str2).positiveText("YA").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                Intent intent = new Intent(PresensiFragment.this.getActivity(), DashboardActivity.class);
                intent.putExtra("data", "1");
                PresensiFragment.this.startActivity(intent);
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void DialogPesanExpired(String str, String str2) {
        new MaterialDialog.Builder(this.mContext).title(str).content(str2).positiveText("Refresh").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                Intent intent = new Intent(PresensiFragment.this.mContext, DashboardActivity.class);
                intent.putExtra("data", "2");
                PresensiFragment.this.startActivity(intent);
            }
        }).show().setCanceledOnTouchOutside(false);
    }

    private void ReqList() {
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_ReqLokasi, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d(PresensiFragment.TAG, str);
                PresensiFragment.this.hidePDialog();
                PresensiFragment.this.swipeLayout.setRefreshing(false);
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    String string = jSONObject.getString("response");
                    if (string.equals("1")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
                        if (jSONObject2.getString("libur").equals("1")) {
                            PresensiFragment.this.LLibur.setVisibility(0);
                            return;
                        }
                        JSONArray jSONArray = jSONObject2.getJSONArray("schedule");
                        if (jSONObject2.getString("status_dinas").equals("0")) {
                            PresensiFragment.this.LLokasi.setVisibility(8);
                        } else {
                            PresensiFragment.this.LLokasi.setVisibility(0);
                            PresensiFragment.this.LLokasi.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    PresensiFragment.this.startActivity(new Intent(PresensiFragment.this.getContext(), InputMapsActivity.class));
                                }
                            });
                        }
                        PresensiFragment.this.LLibur.setVisibility(8);
                        PresensiFragment.this.PasangData(jSONArray);
                    } else if (string.equals("5")) {
                        PresensiFragment.this.DialogPesanGagal("Maaf", "Anda belum Menentukan Lokasi SKPD. Harap Hubungi Admin OPD Terkait");
                    } else if (string.equals("9")) {
                        PresensiFragment.this.userHelper_sqlite.delete("user_member");
                        PresensiFragment.this.startActivity(new Intent(PresensiFragment.this.getActivity(), LoginActivity.class));
                        PresensiFragment.this.getActivity().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                VolleyLog.d(PresensiFragment.TAG, "Error: " + volleyError.getMessage());
                if (!(volleyError instanceof NetworkError) && !(volleyError instanceof ServerError) && !(volleyError instanceof AuthFailureError) && !(volleyError instanceof ParseError) && !(volleyError instanceof NoConnectionError)) {
                    boolean z = volleyError instanceof TimeoutError;
                }
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("api_key", AppConfig.api_key);
                hashMap.put("nip", PresensiFragment.this.userHelper_sqlite.getobject("nip"));
                hashMap.put("loggedin_token", PresensiFragment.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("tanggal", PresensiFragment.this.strtanggal);
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void PasangData(JSONArray jSONArray) {
        String str = "verifikasi";
        String str2 = "lat_now";
        this.latsekarang = Double.valueOf(Double.parseDouble(this.userHelper_sqlite.getlonglat(str2)));
        String str3 = "long_now";
        this.longsekarang = Double.valueOf(Double.parseDouble(this.userHelper_sqlite.getlonglat(str3)));
        try {
            this.ListMenu.clear();
            int i = 0;
            while (i < jSONArray.length()) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                item_data item_data = new item_data();
                if (jSONObject.getString(str) != null) {
                    item_data.setStatVerifikasi(jSONObject.getString(str));
                } else {
                    item_data.setStatVerifikasi("");
                }
                item_data.setJenisAbsen(jSONObject.getString("jenis_absen"));
                item_data.setLat(jSONObject.getString("lat"));
                item_data.setLong(jSONObject.getString("long"));
                item_data.setNamaLok(jSONObject.getString("detail"));
                StringBuilder sb = new StringBuilder();
                sb.append("");
                String str4 = str;
                sb.append(this.latsekarang);
                item_data.setLatNow(sb.toString());
                item_data.setLongNow("" + this.longsekarang);
                Location location = new Location("");
                int i2 = i;
                location.setLatitude(this.latsekarang.doubleValue());
                location.setLongitude(this.longsekarang.doubleValue());
                Log.d("LATSEKARANG : ", "" + this.userHelper_sqlite.getlonglat(str2));
                Log.d("LONGSEKARANG : ", "" + this.userHelper_sqlite.getlonglat(str3));
                Location location2 = new Location("");
                String str5 = str2;
                String str6 = str3;
                location2.setLatitude(Double.parseDouble(jSONObject.getString("lat")));
                location2.setLongitude(Double.parseDouble(jSONObject.getString("long")));
                Float valueOf = Float.valueOf(location.distanceTo(location2));
                if (((double) valueOf.floatValue()) > 100.0d) {
                    this.LLalert.setVisibility(0);
                } else {
                    this.LLalert.setVisibility(8);
                }
                if (jSONObject.getString("jenis_absen").equals("request")) {
                    item_data.setJarak("10");
                } else {
                    item_data.setJarak("" + valueOf);
                }
                item_data.setStatusAbsenMasuk(jSONObject.getString("status_absen_datang"));
                item_data.setStatusAbsenPulang(jSONObject.getString("status_absen_pulang"));
                if (jSONObject.getString("status_absen_datang").equals("1")) {
                    item_data.setJamDatang(jSONObject.getString("jam_datang"));
                } else {
                    item_data.setJamDatang("00:00");
                }
                if (jSONObject.getString("status_absen_pulang").equals("1")) {
                    item_data.setJamPulang(jSONObject.getString("jam_pulang"));
                } else {
                    item_data.setJamPulang("00:00");
                }
                List<Address> fromLocation = this.geocoder.getFromLocation(Double.parseDouble(jSONObject.getString("lat")), Double.parseDouble(jSONObject.getString("long")), 1);
                String addressLine = fromLocation.get(0).getAddressLine(0);
                fromLocation.get(0).getLocality();
                fromLocation.get(0).getAdminArea();
                fromLocation.get(0).getCountryName();
                fromLocation.get(0).getPostalCode();
                fromLocation.get(0).getFeatureName();
                item_data.setAlamat(addressLine);
                this.ListMenu.add(item_data);
                i = i2 + 1;
                str2 = str5;
                str = str4;
                str3 = str6;
            }
            this.mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    @SuppressLint({"RestrictedApi"})
    private void initlokasi() {
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        this.mSettingsClient = LocationServices.getSettingsClient(getContext());
        this.mLocationCallback = new LocationCallback() {
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location unused = PresensiFragment.this.mCurrentLocation = locationResult.getLastLocation();
                String unused2 = PresensiFragment.this.mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                PresensiFragment.this.updateLocationUI();
            }
        };
        this.mRequestingLocationUpdates = false;
        this.mLocationRequest = new LocationRequest();
        this.mLocationRequest.setInterval(10000);
        this.mLocationRequest.setFastestInterval(10000);
        this.mLocationRequest.setPriority(100);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(this.mLocationRequest);
        this.mLocationSettingsRequest = builder.build();
    }

    private void restoreValuesFromBundle(Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey("is_requesting_updates")) {
                this.mRequestingLocationUpdates = Boolean.valueOf(bundle.getBoolean("is_requesting_updates"));
            }
            if (bundle.containsKey("last_known_location")) {
                this.mCurrentLocation = bundle.getParcelable("last_known_location");
            }
            if (bundle.containsKey("last_updated_on")) {
                this.mLastUpdateTime = bundle.getString("last_updated_on");
            }
        }
        updateLocationUI();
    }

    /* access modifiers changed from: private */
    public void updateLocationUI() {
        if (this.mCurrentLocation != null) {
            this.strlat = "" + this.mCurrentLocation.getLatitude();
            this.strlong = "" + this.mCurrentLocation.getLongitude();
            if (!(this.strlat == null || this.strlong == null)) {
                UserHelper_sqlite userHelper_sqlite2 = this.userHelper_sqlite;
                userHelper_sqlite2.uplonglatnow(userHelper_sqlite2.getobject("nip"), this.strlat, this.strlong);
            }
            Log.d("lokasi sekarang lat :", this.strlat);
            Log.d("lokasi skrg long :", this.strlong);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("is_requesting_updates", this.mRequestingLocationUpdates.booleanValue());
        bundle.putParcelable("last_known_location", this.mCurrentLocation);
        bundle.putString("last_updated_on", this.mLastUpdateTime);
    }

    public void startLocationButtonClick() {
        Dexter.withActivity(getActivity()).withPermission("android.permission.ACCESS_FINE_LOCATION").withListener(new PermissionListener() {
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                Boolean unused = PresensiFragment.this.mRequestingLocationUpdates = true;
                PresensiFragment.this.startLocationUpdates();
            }

            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                if (permissionDeniedResponse.isPermanentlyDenied()) {
                    PresensiFragment.this.openSettings();
                }
            }

            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).check();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 10000) {
            if (i2 == -1) {
                Log.e(TAG, "User agreed to make required location settings changes.");
            } else if (i2 == 0) {
                Log.e(TAG, "User chose not to make required location settings changes.");
                this.mRequestingLocationUpdates = false;
            }
        }
    }

    /* access modifiers changed from: private */
    public void openSettings() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", BuildConfig.APPLICATION_ID, null));
        intent.setFlags(268435456);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void startLocationUpdates() {
        this.mSettingsClient.checkLocationSettings(this.mLocationSettingsRequest).addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
            @SuppressLint({"MissingPermission"})
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.i(PresensiFragment.TAG, "All location settings are satisfied.");
                PresensiFragment.this.mFusedLocationClient.requestLocationUpdates(PresensiFragment.this.mLocationRequest, PresensiFragment.this.mLocationCallback, Looper.myLooper());
                PresensiFragment.this.updateLocationUI();
            }
        }).addOnFailureListener(getActivity(), new OnFailureListener() {
            public void onFailure(@NonNull Exception exc) {
                int statusCode = ((ApiException) exc).getStatusCode();
                if (statusCode == 6) {
                    Log.i(PresensiFragment.TAG, "Location settings are not satisfied. Attempting to upgrade location settings ");
                    try {
                        ((ResolvableApiException) exc).startResolutionForResult(PresensiFragment.this.getActivity(), 10000);
                    } catch (IntentSender.SendIntentException unused) {
                        Log.i(PresensiFragment.TAG, "PendingIntent unable to execute request.");
                    }
                } else if (statusCode == 8502) {
                    Log.e(PresensiFragment.TAG, "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                    Toast.makeText(PresensiFragment.this.getContext(), "Location settings are inadequate, and cannot be fixed here. Fix in Settings.", 1).show();
                }
                PresensiFragment.this.updateLocationUI();
            }
        });
    }

    public void stopLocationUpdates() {
        this.mFusedLocationClient.removeLocationUpdates(this.mLocationCallback).addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
            public void onComplete(@NonNull Task<Void> task) {
            }
        });
    }

    public void onResume() {
        super.onResume();
        if (this.mRequestingLocationUpdates.booleanValue() && checkPermissions()) {
            startLocationUpdates();
            keepServicesInChineseDevices();
        }
        updateLocationUI();
    }

    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(getContext(), "android.permission.ACCESS_FINE_LOCATION") == 0;
    }

    public void onPause() {
        super.onPause();
        this.countDownTimer.cancel();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void keepServicesInChineseDevices() {
        /*
            r6 = this;
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            java.lang.String r1 = android.os.Build.MANUFACTURER
            int r2 = r1.hashCode()
            r3 = -759499589(0xffffffffd2baf4bb, float:-4.01484906E11)
            r4 = 2
            r5 = 1
            if (r2 == r3) goto L_0x0031
            r3 = 3418016(0x3427a0, float:4.78966E-39)
            if (r2 == r3) goto L_0x0027
            r3 = 3620012(0x373cac, float:5.072717E-39)
            if (r2 == r3) goto L_0x001d
            goto L_0x003b
        L_0x001d:
            java.lang.String r2 = "vivo"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x003b
            r1 = 2
            goto L_0x003c
        L_0x0027:
            java.lang.String r2 = "oppo"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x003b
            r1 = 1
            goto L_0x003c
        L_0x0031:
            java.lang.String r2 = "xiaomi"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x003b
            r1 = 0
            goto L_0x003c
        L_0x003b:
            r1 = -1
        L_0x003c:
            if (r1 == 0) goto L_0x005d
            if (r1 == r5) goto L_0x0050
            if (r1 == r4) goto L_0x0043
            goto L_0x0069
        L_0x0043:
            android.content.ComponentName r1 = new android.content.ComponentName
            java.lang.String r2 = "com.vivo.permissionmanager"
            java.lang.String r3 = "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
            r1.<init>(r2, r3)
            r0.setComponent(r1)
            goto L_0x0069
        L_0x0050:
            android.content.ComponentName r1 = new android.content.ComponentName
            java.lang.String r2 = "com.coloros.safecenter"
            java.lang.String r3 = "com.coloros.safecenter.permission.startup.StartupAppListActivity"
            r1.<init>(r2, r3)
            r0.setComponent(r1)
            goto L_0x0069
        L_0x005d:
            android.content.ComponentName r1 = new android.content.ComponentName
            java.lang.String r2 = "com.miui.securitycenter"
            java.lang.String r3 = "com.miui.permcenter.autostart.AutoStartManagementActivity"
            r1.<init>(r2, r3)
            r0.setComponent(r1)
        L_0x0069:
            androidx.fragment.app.FragmentActivity r1 = r6.getActivity()
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            r2 = 65536(0x10000, float:9.18355E-41)
            r1.queryIntentActivities(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: nzhi.dnt.epresensiblora.fragment.PresensiFragment.keepServicesInChineseDevices():void");
    }

    public static double distFrom(Double d, Double d2, float f, float f2) {
        double d3 = (double) f;
        double doubleValue = d.doubleValue();
        Double.isNaN(d3);
        double radians = Math.toRadians(d3 - doubleValue);
        double d4 = (double) f2;
        double doubleValue2 = d2.doubleValue();
        Double.isNaN(d4);
        double d5 = radians / 2.0d;
        double radians2 = Math.toRadians(d4 - doubleValue2) / 2.0d;
        double sin = (Math.sin(d5) * Math.sin(d5)) + (Math.cos(Math.toRadians(d.doubleValue())) * Math.cos(Math.toRadians(d3)) * Math.sin(radians2) * Math.sin(radians2));
        return Math.atan2(Math.sqrt(sin), Math.sqrt(1.0d - sin)) * 2.0d * 6371000.0d;
    }
}
