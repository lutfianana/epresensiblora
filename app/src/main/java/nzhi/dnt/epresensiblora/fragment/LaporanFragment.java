package nzhi.dnt.epresensiblora.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nzhi.dnt.epresensiblora.R;
import nzhi.dnt.epresensiblora.activity.LoginActivity;
import nzhi.dnt.epresensiblora.adapter.adapter_laporan;
import nzhi.dnt.epresensiblora.adapter.adapter_laporan_tahunan;
import nzhi.dnt.epresensiblora.bridge.AppConfig;
import nzhi.dnt.epresensiblora.bridge.AppController;
import nzhi.dnt.epresensiblora.helper.UserHelper_sqlite;
import nzhi.dnt.epresensiblora.model.item_data;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LaporanFragment extends Fragment {
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    /* access modifiers changed from: private */
    public static String TAG = "LaporanFragment";
    static Dialog d;
    private String[] Item = {"Pilih", "Pertanggal", "Perbulan", "Pertahun"};
    private String[] Item2 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    LinearLayout LLBulan;
    LinearLayout LLTahun;
    LinearLayout LLtanggal;
    LinearLayout LLtotalbulan;
    private List<item_data> ListMenu = new ArrayList();
    /* access modifiers changed from: private */
    public RecyclerView RVLaporan;
    Button btnoke;
    private DatePickerDialog datePickerDialog;
    SimpleDateFormat dbulan;
    SimpleDateFormat df;
    SimpleDateFormat dtahun;
    EditText edtanggal1;
    EditText edtanggal2;
    adapter_laporan mAdapter;
    adapter_laporan_tahunan mAdapter2;
    protected LayoutManagerType mCurrentLayoutManagerType;
    protected RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog pDialog;
    View rootView;
    Spinner spbulan;
    String strbulan;
    String strbulannow;
    String strnip;
    String strtahun;
    String strtglnow;
    Date today;
    TextView tvabsen;
    TextView tvhadir;
    TextView tvtahun;
    UserHelper_sqlite userHelper_sqlite;
    int year = Calendar.getInstance().get(1);

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.rootView = layoutInflater.inflate(R.layout.fragment_laporan, viewGroup, false);
        Spinner spinner = this.rootView.findViewById(R.id.splist);
        final Spinner spinner2 = this.rootView.findViewById(R.id.spbulan);
        this.userHelper_sqlite = new UserHelper_sqlite(getActivity().getApplication());
        this.df = new SimpleDateFormat("yyyy-MM-dd");
        this.dbulan = new SimpleDateFormat("MM");
        this.dtahun = new SimpleDateFormat("yyyy");
        this.today = Calendar.getInstance().getTime();
        this.strbulannow = this.dbulan.format(this.today);
        this.strtahun = this.dtahun.format(this.today);
        this.LLtanggal = this.rootView.findViewById(R.id.LLtanggal);
        this.LLBulan = this.rootView.findViewById(R.id.LLbulan);
        this.LLTahun = this.rootView.findViewById(R.id.LLTahun);
        this.LLtotalbulan = this.rootView.findViewById(R.id.LLTotal);
        this.tvabsen = this.rootView.findViewById(R.id.tvtotalabsen);
        this.tvtahun = this.rootView.findViewById(R.id.edtahun);
        this.tvhadir = this.rootView.findViewById(R.id.tvtotalhadir);
        this.pDialog = new ProgressDialog(getActivity());
        this.edtanggal1 = this.rootView.findViewById(R.id.edtanggal1);
        this.edtanggal2 = this.rootView.findViewById(R.id.edtanggal2);
        this.RVLaporan = this.rootView.findViewById(R.id.RVlaporan);
        this.btnoke = this.rootView.findViewById(R.id.btnok);
        this.LLtotalbulan.setVisibility(8);
        this.RVLaporan = this.rootView.findViewById(R.id.RVlaporan);
        final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), 17367049, this.Item2);
        spinner2.setAdapter(arrayAdapter);
        final ArrayAdapter arrayAdapter2 = new ArrayAdapter(getActivity(), 17367049, this.Item);
        spinner.setAdapter(arrayAdapter2);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                String obj = arrayAdapter2.getItem(i).toString();
                if (obj.equals("Pilih")) {
                    LaporanFragment.this.LLtanggal.setVisibility(8);
                    LaporanFragment.this.LLBulan.setVisibility(8);
                    LaporanFragment.this.LLtotalbulan.setVisibility(8);
                    LaporanFragment.this.LLTahun.setVisibility(8);
                    LaporanFragment.this.btnoke.setVisibility(8);
                    LaporanFragment.this.RVLaporan.setVisibility(8);
                    LaporanFragment laporanFragment = LaporanFragment.this;
                    laporanFragment.ReqLaporanBulan(laporanFragment.strbulannow);
                } else if (obj.equals("Pertanggal")) {
                    LaporanFragment.this.LLtanggal.setVisibility(0);
                    LaporanFragment.this.LLBulan.setVisibility(8);
                    LaporanFragment.this.LLtotalbulan.setVisibility(8);
                    LaporanFragment.this.LLTahun.setVisibility(8);
                    LaporanFragment.this.btnoke.setVisibility(0);
                    LaporanFragment.this.edtanggal1.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            LaporanFragment.this.OpenDatePicker("tanggal");
                        }
                    });
                    LaporanFragment.this.edtanggal2.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            LaporanFragment.this.OpenDatePicker("tanggal_akhir");
                        }
                    });
                    LaporanFragment.this.btnoke.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            LaporanFragment.this.ReqLaporanTanggal(LaporanFragment.this.edtanggal1.getText().toString(), LaporanFragment.this.edtanggal2.getText().toString());
                        }
                    });
                    LaporanFragment.this.RVLaporan.setVisibility(8);
                } else if (obj.equals("Perbulan")) {
                    LaporanFragment.this.LLtanggal.setVisibility(8);
                    LaporanFragment.this.LLBulan.setVisibility(0);
                    LaporanFragment.this.LLTahun.setVisibility(8);
                    LaporanFragment.this.RVLaporan.setVisibility(8);
                    LaporanFragment.this.btnoke.setVisibility(0);
                    spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }

                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                            LaporanFragment.this.strbulan = arrayAdapter.getItem(i).toString();
                        }
                    });
                    LaporanFragment.this.btnoke.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            LaporanFragment.this.ReqLaporanBulan(LaporanFragment.this.strbulan);
                        }
                    });
                } else if (obj.equals("Pertahun")) {
                    LaporanFragment.this.LLtanggal.setVisibility(8);
                    LaporanFragment.this.LLBulan.setVisibility(8);
                    LaporanFragment.this.LLtotalbulan.setVisibility(8);
                    LaporanFragment.this.LLTahun.setVisibility(0);
                    LaporanFragment.this.LLTahun.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            LaporanFragment.this.showYearDialog();
                        }
                    });
                    LaporanFragment.this.RVLaporan.setVisibility(8);
                    LaporanFragment.this.btnoke.setVisibility(0);
                    LaporanFragment.this.btnoke.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            LaporanFragment.this.ReqLaporanTahun(LaporanFragment.this.tvtahun.getText().toString());
                        }
                    });
                }
            }
        });
        return this.rootView;
    }

    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void showPDialog(String str) {
        if (!this.pDialog.isShowing()) {
            this.pDialog.setMessage(str);
        }
        this.pDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePDialog() {
        if (this.pDialog.isShowing()) {
            this.pDialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void ReqLaporanTanggal(String str, String str2) {
        showPDialog("Loading ..");
        final String str3 = str;
        final String str4 = str2;
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_LaporanTanggal, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                LaporanFragment.this.hidePDialog();
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    String string = jSONObject.getString("response");
                    if (string.equals("1")) {
                        LaporanFragment.this.PasangData(jSONObject.getJSONObject("data").getJSONArray("absensi"));
                    } else if (string.equals("9")) {
                        LaporanFragment.this.userHelper_sqlite.delete("user_member");
                        LaporanFragment.this.startActivity(new Intent(LaporanFragment.this.getActivity(), LoginActivity.class));
                        LaporanFragment.this.getActivity().finish();
                    } else {
                        LaporanFragment.this.DialogPesanGagal("Gagal", "Maaf Terjadi Kesalahan Silakan Ulangi");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$800 = LaporanFragment.TAG;
                VolleyLog.d(access$800, "Error: " + volleyError.getMessage());
                LaporanFragment.this.hidePDialog();
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("nip", LaporanFragment.this.userHelper_sqlite.getobject("nip"));
                hashMap.put("tanggal_awal", str3);
                hashMap.put("tanggal_akhir", str4);
                hashMap.put("loggedin_token", LaporanFragment.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("api_key", AppConfig.api_key);
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void ReqLaporanBulan(String str) {
        showPDialog("Loading ..");
        final String str2 = str;
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_LaporanBulan, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                LaporanFragment.this.hidePDialog();
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    String string = jSONObject.getString("response");
                    if (string.equals("1")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
                        JSONArray jSONArray = jSONObject2.getJSONArray("absensi");
                        String string2 = jSONObject2.getString("hadir");
                        String string3 = jSONObject2.getString("tanpa_keterangan");
                        LaporanFragment.this.LLtotalbulan.setVisibility(0);
                        LaporanFragment.this.tvabsen.setText(string3);
                        LaporanFragment.this.tvhadir.setText(string2);
                        LaporanFragment.this.PasangData(jSONArray);
                    } else if (string.equals("9")) {
                        LaporanFragment.this.userHelper_sqlite.delete("user_member");
                        LaporanFragment.this.startActivity(new Intent(LaporanFragment.this.getActivity(), LoginActivity.class));
                        LaporanFragment.this.getActivity().finish();
                    } else {
                        LaporanFragment.this.DialogPesanGagal("Gagal", "Maaf Terjadi Kesalahan Silakan Ulangi");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$800 = LaporanFragment.TAG;
                VolleyLog.d(access$800, "Error: " + volleyError.getMessage());
                LaporanFragment.this.hidePDialog();
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("nip", LaporanFragment.this.userHelper_sqlite.getobject("nip"));
                hashMap.put("bulan", str2);
                hashMap.put("tahun", LaporanFragment.this.strtahun);
                hashMap.put("loggedin_token", LaporanFragment.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("api_key", AppConfig.api_key);
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void ReqLaporanTahun(String str) {
        showPDialog("Loading ..");
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_LaporanTahun, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                LaporanFragment.this.hidePDialog();
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    if (jSONObject.getString("response").equals("1")) {
                        LaporanFragment.this.PasangDataTahun(jSONObject.getJSONObject("data"));
                        return;
                    }
                    LaporanFragment.this.DialogPesanGagal("Gagal", "Maaf Terjadi Kesalahan Silakan Ulangi");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$800 = LaporanFragment.TAG;
                VolleyLog.d(access$800, "Error: " + volleyError.getMessage());
                LaporanFragment.this.hidePDialog();
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("nip", LaporanFragment.this.userHelper_sqlite.getobject("nip"));
                hashMap.put("tahun", LaporanFragment.this.tvtahun.getText().toString());
                hashMap.put("loggedin_token", LaporanFragment.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("api_key", AppConfig.api_key);
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void DialogPesanGagal(String str, String str2) {
        new MaterialDialog.Builder(getActivity()).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void OpenDatePicker(final String str) {
        Calendar instance = Calendar.getInstance();
        this.datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                Calendar instance = Calendar.getInstance();
                instance.set(i, i2, i3);
                if (str.equals("tanggal")) {
                    LaporanFragment.this.edtanggal1.setText(LaporanFragment.this.df.format(instance.getTime()));
                } else if (str.equals("tanggal_akhir")) {
                    LaporanFragment.this.edtanggal2.setText(LaporanFragment.this.df.format(instance.getTime()));
                }
            }
        }, instance.get(1), instance.get(2), instance.get(5));
        this.datePickerDialog.show();
    }

    public void showYearDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setTitle("Pilih Tahun");
        dialog.setContentView(R.layout.yearpicker);
        TextView textView = this.tvtahun;
        textView.setText("" + this.year);
        final NumberPicker numberPicker = dialog.findViewById(R.id.numberPicker1);
        numberPicker.setMaxValue(this.year + 50);
        numberPicker.setMinValue(this.year + -50);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setValue(this.year);
        numberPicker.setDescendantFocusability(393216);
        dialog.findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
                LaporanFragment.this.tvtahun.setText(String.valueOf(numberPicker.getValue()));
            }
        });
        dialog.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void PasangDataTahun(JSONObject jSONObject) {
        this.ListMenu.clear();
        for (int i = 0; i < 13; i++) {
            item_data item_data = new item_data();
            item_data.setNamaBulan("" + i);
            try {
                JSONObject jSONObject2 = jSONObject.getJSONObject("" + i);
                for (int i2 = 0; i2 < jSONObject2.length(); i2++) {
                    item_data.setJumlahAbsen(jSONObject2.getString("tanpa_keterangan"));
                    item_data.setJumlahMasuk(jSONObject2.getString("hadir"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.ListMenu.add(item_data);
        }
        this.RVLaporan.setVisibility(0);
        this.RVLaporan.setHasFixedSize(true);
        this.mLayoutManager = new GridLayoutManager(getContext(), 1);
        this.RVLaporan.setLayoutManager(this.mLayoutManager);
        this.mAdapter2 = new adapter_laporan_tahunan(getContext(), this.ListMenu);
        this.RVLaporan.setAdapter(this.mAdapter2);
        this.mAdapter2.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void PasangData(JSONArray jSONArray) {
        try {
            this.ListMenu.clear();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                item_data item_data = new item_data();
                if (jSONObject.getString("libur").equals("1")) {
                    item_data.setTanggal(jSONObject.getString("tanggal"));
                    item_data.setJamDatang("00.00");
                    item_data.setJamPulang("00.00");
                    item_data.setJenisMasuk("");
                    item_data.setJenisPulang("");
                } else {
                    item_data.setTanggal(jSONObject.getString("tanggal"));
                    item_data.setJamDatang(jSONObject.getString("jam_datang"));
                    item_data.setJamPulang(jSONObject.getString("jam_pulang"));
                    item_data.setJenisMasuk(jSONObject.getString("mode_datang"));
                    item_data.setJenisPulang(jSONObject.getString("mode_pulang"));
                }
                this.ListMenu.add(item_data);
            }
            this.RVLaporan.setVisibility(0);
            this.RVLaporan.setHasFixedSize(true);
            this.mLayoutManager = new GridLayoutManager(getContext(), 1);
            this.RVLaporan.setLayoutManager(this.mLayoutManager);
            this.mAdapter = new adapter_laporan(getContext(), this.ListMenu);
            this.RVLaporan.setAdapter(this.mAdapter);
            this.mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
